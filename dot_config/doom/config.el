;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;       Personal Information       ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq user-full-name "Stefan Greiß"
      user-mail-address "stefan.greiss@mailbox.org")

;;; use pass (~/.password-store)
;;; (see The Unix password store)
(setq auth-sources '("~/.local/share/password-store"))

;; TODO setup file VERIFY
;; (setq auth-sources '("~/.authinfo.gpg"))
      ;; auth-source-cache-expiry nil) ; default is 7200 (2h)
;; (setq epg-gpg-program "/usr/bin/gpg2")
;; (require 'epa-file)
;; (epa-file-enable)
;; (setq epa-file-select-keys nil)
;; (setq epa-pinentry-mode 'loopback)
(setenv "GPG_AGENT_INFO" nil)
;; (setq auth-sources
;; '((:source "~/.config/doom/secrets/.authinfo.gpg")))

(setq auth-sources '("~/.authinfo.gpg"))
(setq epa-pinentry-mode 'loopback)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;       Simple Settings       ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq-default
 ;; delete-by-moving-to-trash t                      ; Delete files to trash
 tab-width 4                                      ; Set width for tabs
 ;; uniquify-buffer-name-style 'forward              ; Uniquify buffer names
 ;; window-combination-resize t                      ; take new window space from all other windows (not just current)
 x-stretch-cursor t)                              ; Stretch cursor to the glyph width

(setq undo-limit 10000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t                       ; By default while in insert all changes are one big blob. Be more granular
      auto-save-default t                         ; Nobody likes to loose work, I certainly don't
      inhibit-compacting-font-caches t            ; When there are lots of glyphs, keep them in memory
      truncate-string-ellipsis "…")               ; Unicode ellispis are nicer than "...", and also save /precious/ space

;; (delete-selection-mode 1)                         ; Replace selection when inserting text
;; (display-time-mode 1)                             ; Enable time in the mode-line
;; (unless (equal "Battery status not available"
;;                (battery))
;;   (display-battery-mode 1))                       ; On laptops it's nice to know how much power you have
(global-subword-mode 1)                           ; Iterate through CamelCase words

;; Multiplexing emacs and emacsclient
(require 'server)
(unless (server-running-p)
  (server-start))

(setq default-buffer-file-coding-system 'utf-8-unix)
;; my env
; (doom-load-envvars-file "$DOOMDIR/myenv")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;       Doom config      ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Font face
;; TODO check doom docs font config
(setq doom-font (font-spec :family "FiraCode Nerd Font" :size 15))
;; (setq doom-font (font-spec :family "FireaCode Mono" :size 24)
;;       doom-big-font (font-spec :family "FireaCode Mono" :size 36)
;;       doom-variable-pitch-font (font-spec :family "Overpass" :size 24)
;;       doom-serif-font (font-spec :family "IBM Plex Mono" :weight 'light))


;; Theme and modeline
(setq doom-theme 'doom-gruvbox)

;; Miscellaneous
(setq display-line-numbers-type 'relative)

(setq org-default-notes-file (concat org-directory "/inbox.org"))
;; (setq projectile-project-search-path "~/")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;       PACKAGES       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (require 'package) or use-package
;; doom modules VERIFY if doom modules need to be required
;; custom packages
;; (require 'magit-svn)
;; (require 'prettier-js)


;; web
(use-package! web-mode
  :config
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2))

;; Add docsets to minor modes by starting DOCSETS with :add
(set-docsets! 'rjsx-mode :add "React")
;; (setq web-mode-engines-alist
;;              '(("php" . "\\.phtml\\'")
;;                ("jinja" . "\\.djhtml\\'")))

;; prettier
(use-package! prettier-js
  :hook
  (rjsx-mode . prettier-js-mode)
  (js2-mode . prettier-js-mode)
  (web-mode . prettier-js-mode)
  (+web-angularjs-mode . prettier-js-mode)
  (typescript-mode . prettier-js-mode)
  (php-mode . prettier-js-mode)
  :config
  (setq prettier-js-args
        '("--print-width" "120"
          "--tab-width" "2"
          "--use-tabs" "false"
          "--arrow-parens" "always"
          "--bracket-spacing" "true"
          "--jsx-single-quote" "true"
          "--trailing-comma" "es5"
          ;; "--jsx-bracket-same-line" "false"
          "--end-of-line" "lf"
          "--insert-pragma" "false"
          "--require-pragma" "false"
          "--prose-wrap" "preserve"
          "--quote-props" "as-needed"
          "--semi" "true"
          ;; php config
          "--php-version" "7.4"
          "--print-width" "120"
          "--tab-width" "4"
          "--use-tabs" "false"
          "--single-quote" "true"
          "--trailing-comma-php" "true"
          "--brace-style" "psr-2"
          "--require-pragma" "false"
          "--insert-pragma" "false")))


(use-package! sqlformat
  :defer t
  :config
  (setq sqlformat-command 'sqlfluff)
  ;; (setq sqlformat-args '("-k" "upper"
  ;;                        "-s2" "-g"))
  (add-hook 'sql-mode-hook 'sqlformat-on-save-mode)
  (map! :localleader
        :map sql-mode-map

        (:desc "Format" "f" 'sqlformat)))

;; GdScript
;;;; Known issues
(defun lsp--gdscript-ignore-errors (original-function &rest args)
  "Ignore the error message resulting from Godot not replying to the `JSONRPC' request."
  (if (string-equal major-mode "gdscript-mode")
      (let ((json-data (nth 0 args)))
        (if (and (string= (gethash "jsonrpc" json-data "") "2.0")
                 (not (gethash "id" json-data nil))
                 (not (gethash "method" json-data nil)))
            nil ; (message "Method not found")
          (apply original-function args)))
    (apply original-function args)))
;; Runs the function `lsp--gdscript-ignore-errors` around `lsp--get-message-type` to suppress unknown notification errors.
(advice-add #'lsp--get-message-type :around #'lsp--gdscript-ignore-errors)

;;;; Customization
(use-package! gdscript-mode
  :defer t
  :config
  (set-lookup-handlers! 'gdscript-mode
    :documentation #'gdscript-docs-browse-symbol-at-point)
  (setq gdscript-gdformat-save-and-format t)
  (setq gdscript-docs-local-path "/home/zythaar/documents/docs/godot")
  (add-hook 'after-save-hook #'gdscript-format-buffer)

  (when (featurep! +lsp)
    (add-hook 'gdscript-mode-local-vars-hook #'lsp!)
    ;; runs the function `lsp--gdscript-ignore-errors` around `lsp--get-message-type` to suppress unknown notification errors.
    (advice-add #'lsp--get-message-type :around #'lsp--gdscript-ignore-errors)))


;; Haskel
(after! ccls
  (setq ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t)))
  (set-lsp-priority! 'ccls 2)) ; optional as ccls is the default in Doom

;; (after!
;;   (setq lsp-haskell-formatting-provider "britany"))


;; Magit svn:
(use-package! magit-svn
  :defer t
  :hook (magit-mode . magit-svn-mode)
  :config
  (map! :localleader
        :map magit-svn-mode-map

        (:desc "Fetch" "f" #'magit-svn-fetch
         :desc "Externals" "e" #'magit-svn-fetch-externals
         :desc "Rebase" "r" #'magit-svn-rebase
         :desc "Commit" "d" #'magit-svn-dcommit
         :desc "Show commit" "c" #'magit-svn-show-commit)

        (:prefix ("m" . "misc")
         :desc "Get ref r" "r" #'magit-svn-get-ref
         :desc "Get rev" "v" #'magit-svn-get-rev
         :desc "Get url" "u" #'magit-svn-get-url
         :desc "Get ref info" "i" #'magit-svn-get-ref-info
         :desc "Get local ref" "l" #'magit-svn-get-local-ref)

        (:prefix ("b" . "branch")
         :desc "Create branch" "b" #'magit-svn-create-branch
         :desc "Create tag" "t" #'magit-svn-create-tag)))


(use-package! lorem-ipsum
  :defer t)
;; ledger
;; (use-package! legder-mode
;;   :defer t
;;   :hook (legder-mode . flycheck-legder)
;;   :config
;;   ;; (setq legder-mode-files
;;   ;;            '(("\\.dat\\'")
;;   ;;              ("\\.legder\\"))))
;; )


;; TODO sort out this crap: (maby link to config s.h.)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; (setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;; (setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
