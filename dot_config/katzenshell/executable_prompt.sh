#!/bin/bash

# Bail out early if non-interactive
case $- in
  *i*) ;;
    *) return;;
esac

SHORT_HOST=${HOST/.*/}

source "${KATZENSHELL}/git-prompt.sh"
source "${KATZENSHELL}/colors.theme.sh"
source "${KATZENSHELL}/base.theme.sh"
source "${KATZENSHELL}/katzen.theme.sh"

if [[ $PROMPT ]]; then
    export PS1="\["$PROMPT"\]"
fi
